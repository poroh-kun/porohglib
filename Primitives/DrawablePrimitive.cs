﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Primitives
{
    public abstract class DrawablePrimitive
    {
        public Color Color { get; protected set; }
        public Color BorderColor { get; protected set; }

        protected VertexPositionColor[] BoundVertex;

        public virtual void Draw(GraphicsDevice gd)
        {
            gd.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.LineStrip, BoundVertex, 0, BoundVertex.Length - 1);
        }
    }
}
