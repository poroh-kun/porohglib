﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PorohGLib;

namespace PorohGLib.Primitives
{
    public class Line
    {
        public const float _Eps = 0.000001f;

        public Vector2 A { get; private set; }
        public Vector2 B { get; private set; }

        public Line(Vector2 a, Vector2 b)
        {
            A = a;
            B = b;
        }

        public Line(float ax, float ay, float bx, float by)
            : this(new Vector2(ax, ay), new Vector2(bx, by)) { }

        public bool IntersectsBy(Line line)
        {
            float v1 = Vector2Multiply(line.B - line.A, A - line.A);
            float v2 = Vector2Multiply(line.B - line.A, B - line.A);
            float v3 = Vector2Multiply(B - A, line.A - A);
            float v4 = Vector2Multiply(B - A, line.B - A);

            if (RealLess(v1 * v2, 0) && RealLess(v3 * v4, 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool Intersects(Line A, Line B)
        {
            return A.IntersectsBy(B);
        }

        public bool RealLess(float a, float b)
        {
            return b - a > 0;// _Eps;
        }

        public float Vector2Multiply(Vector2 v1, Vector2 v2)
        {
            return v1.X * v2.Y - v2.X * v1.Y;
        }
        //------------------
        public static bool RealEq(float a, float b)
        {
            return Math.Abs(a-b)<=_Eps;
        }

        /// <summary>
        /// пределение координат точки пересечения двух линий. линии заданы уравнением ax + by + c = 0
        /// </summary>
        /// <returns>Значение функции равно true,
        /// если точка пересечения  есть,  и false, если прямые параллельны.</returns>
        public static bool LineToPoint(Vector3 A, Vector3 B, out Vector2 intersect)
        {
            float d = A.X * B.Y - A.Y * B.X;
            if (!RealEq(d, 0))
            {
                float dx = -A.Z * B.Y + A.Y * B.Z;
                float dy = -A.X * B.Z + A.Z * B.X;
                float x = dx / d;
                float y = dy / d;
                intersect = new Vector2(x, y);
                return true;
            }
            else
            {
                intersect = Vector2.Zero;
                return false;
            }
        }

        public static bool LineToPoint(Line A, Line B, out Vector2 intersect)
        {
            Vector2 inter;
            bool result = LineToPoint(LineToEquation(A), LineToEquation(B), out inter);
            intersect = inter;
            return result;
        }
        //----------------------
        /// <summary>
        /// Определение уравнения прямой по координатам двух точек
        /// </summary>
        public static Vector3 LineToEquation(Line line)
        {
            float a = line.B.Y - line.A.Y;
            float b = line.A.X - line.B.X;
            float c = -line.A.X * (line.B.Y - line.A.Y) + line.A.Y * (line.B.X - line.A.X);
            return new Vector3(a, b, c);
        }

        #region operators

        public static bool operator ==(Line p1, Line p2)
        {
            return p1.A == p2.A && p1.B == p2.B;
        }

        public static bool operator !=(Line p1, Line p2)
        {
            return !(p1 == p2);
        }

        public override bool Equals(object obj)
        {
            Line o = (Line)obj;
            return this == o;
        }

        public override int GetHashCode()
        {
            return A.GetHashCode() * 2 + B.GetHashCode();
        }

        public override string ToString()
        {
            return A.ToString() + "-" + B.ToString();
        }

        #endregion
    }
}
