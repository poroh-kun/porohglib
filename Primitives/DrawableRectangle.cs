﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Primitives
{
    public class DrawableRectangle : DrawablePrimitive
    {
        public RectangleF Rectangle { get; private set; }
        private VertexPositionColor[] Vertex;

        public DrawableRectangle(RectangleF origin, Color color, Color borderColor)
        {
            Rectangle = origin;
            Color = color;
            BorderColor = borderColor;
            
            Vertex = new VertexPositionColor[6];

            Vertex[0].Position = new Vector3(Rectangle.Left, Rectangle.Top, 0);
            Vertex[1].Position = new Vector3(Rectangle.Right, Rectangle.Top, 0);
            Vertex[2].Position = new Vector3(Rectangle.Right, Rectangle.Bottom, 0);
            Vertex[3].Position = new Vector3(Rectangle.Right, Rectangle.Bottom, 0);
            Vertex[4].Position = new Vector3(Rectangle.Left, Rectangle.Bottom, 0);
            Vertex[5].Position = new Vector3(Rectangle.Left, Rectangle.Top, 0);

            Vertex[0].Color = Color;
            Vertex[1].Color = Color;
            Vertex[2].Color = Color;
            Vertex[3].Color = Color;
            Vertex[4].Color = Color;
            Vertex[5].Color = Color;

            BoundVertex = new VertexPositionColor[5];

            BoundVertex[0].Position = new Vector3(Rectangle.Left, Rectangle.Top, 0);
            BoundVertex[1].Position = new Vector3(Rectangle.Right, Rectangle.Top, 0);
            BoundVertex[2].Position = new Vector3(Rectangle.Right, Rectangle.Bottom, 0);
            BoundVertex[3].Position = new Vector3(Rectangle.Left, Rectangle.Bottom, 0);
            BoundVertex[4].Position = new Vector3(Rectangle.Left, Rectangle.Top, 0);

            BoundVertex[0].Color = BorderColor;
            BoundVertex[1].Color = BorderColor;
            BoundVertex[2].Color = BorderColor;
            BoundVertex[3].Color = BorderColor;
            BoundVertex[4].Color = BorderColor;
        }

        public DrawableRectangle(RectangleF origin, Color color)
            : this(origin, color, Color.Black) { }

        public DrawableRectangle(RectangleF origin)
            : this(origin, Color.White, Color.Black) { }

        public override void Draw(GraphicsDevice gd)
        {
            gd.DrawUserPrimitives<VertexPositionColor>(PrimitiveType.TriangleList, Vertex, 0, Vertex.Length/3);
            base.Draw(gd);
        }

    }
}
