﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Primitives
{
    public class DrawableLine : DrawablePrimitive
    {
        public const float _Eps = 0.000001f;

        public Vector2 A { get; private set; }
        public Vector2 B { get; private set; }

        public DrawableLine(Vector2 a, Vector2 b, Color color)
        {
            A = a;
            B = b;
            Color = color;
            BoundVertex = new VertexPositionColor[2];
            BoundVertex[0].Position = new Vector3(A, 0);
            BoundVertex[0].Color = color;
            BoundVertex[1].Position = new Vector3(B, 0);
            BoundVertex[1].Color = color;
        }

        public DrawableLine(Vector2 a, Vector2 b)
            : this(a, b, Color.Black) { }

        public DrawableLine(float ax, float ay, float bx, float by, Color color)
            : this(new Vector2(ax, ay), new Vector2(bx, by), color) { }

        public DrawableLine(float ax, float ay, float bx, float by)
            : this(ax, ay, bx, by, Color.Black) { }

    }
}
