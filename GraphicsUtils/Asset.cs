﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorohGLib.Resources.Graphics
{
    public class Asset
    {
        private Sprite[] Sprites;

        public Asset(Sprite[] sprites)
        {
            Sprites = sprites;
        }

        public Sprite this[int i]
        {
            get
            {
                if (i >= Sprites.Length)
                    return Sprites.Last();
                else
                    return Sprites[i];
            }
        }
    }
}
