using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.States;

namespace PorohGLib
{
    public class PGame : Microsoft.Xna.Framework.Game
    {
        private Random _Rnd;
        private List<int> _RndItems;
        public int Rnd
        {
            get
            {
                int rnd = _Rnd.Next(int.MaxValue);
                do
                {
                    rnd = _Rnd.Next(int.MaxValue);
                }
                while (_RndItems.Contains(rnd));
                _RndItems.Add(rnd);
                return rnd;
            }
        }
        public Random Random { get { return _Rnd; } }

        public int Width { get { return GraphicsDevice.DisplayMode.Width; } }
        public int Height { get { return GraphicsDevice.DisplayMode.Height; } }

        public SpriteFont CandaraFont { get; private set; }
        public SpriteFont SmallFont { get; private set; }

        public ResourseManager Resourses { get; private set; }
        public GameStateManager States { get; private set; }
        public GameConfig Config { get; private set; }

        public double FPS { get { return States.FPS.Ticks; } }
        public double UPS { get { return States.UPS.Ticks; } }

        protected GraphicsDeviceManager Graphics;
        protected SpriteBatch SpriteBatch;

        public PGame()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _Rnd = new Random(DateTime.Now.TimeOfDay.Milliseconds);
            _RndItems = new List<int>();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        protected void InitializeConfig(List<ConfigParam> schema, string filename = "config.ini")
        {
            Config = new GameConfig(schema, filename);
        }

        protected void ChangeResolution(int width, int height)
        {
            this.Graphics.PreferredBackBufferWidth = width;
            this.Graphics.PreferredBackBufferHeight = height;
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            CandaraFont = Content.Load<SpriteFont>(@"Fonts/Candara");
            SmallFont = Content.Load<SpriteFont>(@"Fonts/font");


            Resourses = new ResourseManager(Content);
            States = new GameStateManager(GraphicsDevice, SpriteBatch, Resourses);
            States.ToExit += Exit;

            Start();
        }

        protected virtual void Start()
        {

        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            States.Update(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            States.Draw(gameTime);

            base.Draw(gameTime);
        }

        public SpriteFont GetFont(string font)
        {
            switch (font)
            {
                case "Candara": return CandaraFont;
                case "Small": return SmallFont;
                default: return CandaraFont;
            }
        }

        public void DrawString(string text, SpriteBatch spriteBatch, string font, Vector2 position, Color color)
        {
            spriteBatch.DrawString(GetFont(font), text, position, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }

    }
}
