﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using PorohGLib.Resources.Graphics;

namespace PorohGLib.Resources
{
    public class ResourseManager
    {
        private List<Texture2D> Textures;
        private Dictionary<string, Dictionary<string, IpImage>> Images;
        private Dictionary<string, SpriteFont> Fonts;

        public static ResourseManager Instance;

        public ResourseManager(ContentManager content)
        {
            Instance = this;
            Textures = new List<Texture2D>();
            Images = new Dictionary<string, Dictionary<string, IpImage>>();
            Fonts = new Dictionary<string, SpriteFont>();
            
            XElement xAnimations = new XElement("animations");
            
            string[] files = System.IO.Directory.GetFiles(content.RootDirectory, "*.xml",
                System.IO.SearchOption.AllDirectories);

            foreach (string file in files)
            {
                XElement Xel = XElement.Load(file);
                foreach (XElement xel in Xel.Elements("content"))
                {
                    switch (xel.Attribute("type").Value.ToString())
                    {
                        case "sprites":
                            {
                                Texture2D texture = content.Load<Texture2D>(xel.Attribute("file").Value);
                                AddTextures(texture, xel);
                                break;
                            }
                        case "animations":
                            foreach (XElement xl in xel.Elements("animation"))
                                xAnimations.Add(xl);
                            break;
                        case "font":
                            SpriteFont font = content.Load<SpriteFont>(xel.Attribute("file").Value);
                            AddFont(font, xel.Element("font"));
                            break;
                    }
                }
            }
            foreach (XElement xel in xAnimations.Elements())
                AddAnimation(xel);
        }

        private void AddTextures(Texture2D texture, XElement set)
        {
            Textures.Add(texture);
            foreach (XElement Xel in set.Elements("tileset"))
            {
                string tilesetName = Xel.Attribute("name").Value;

                foreach (XElement xel in Xel.Elements("tile"))
                {
                    string tileName = xel.Attribute("name").Value;
                    if (!Images.ContainsKey(tilesetName))
                        Images.Add(tilesetName, new Dictionary<string, IpImage>());
                    Images[tilesetName].Add(tileName, new Sprite(texture, tilesetName, xel));
                }
            }
        }

        private void AddAnimation(XElement xel)
        {
            string groupName = xel.Attribute("name").Value.ToString();
            if (!Images.ContainsKey(groupName))
                        Images.Add(groupName, new Dictionary<string, IpImage>());
            foreach (XElement xl in xel.Elements("state"))
            {
                string state = xl.Attribute("name").Value.ToString();
                int duration= int.Parse(xl.Attribute("duration").Value);
                List<Sprite> images = new List<Sprite>();
                foreach (XElement x in xl.Elements("frame"))
                {
                    string[] tile = x.Attribute("tile").Value.ToString().Split('.');
                    images.Add((Sprite)Images[tile[0]][tile[1]]);
                }
                if (images.Count > 0)
                    Images[groupName].Add(state, new Animation(images, duration, groupName, state));
            }
        }

        private void AddFont(SpriteFont font, XElement xFont)
        {
            Fonts.Add(xFont.Attribute("name").Value, font);
        }

        public IpImage GetImage(string group, string name)
        {
            return Images[group][name];
        }

        public SpriteFont GetFont(string name)
        {
            return Fonts[name];
        }

    }
}
