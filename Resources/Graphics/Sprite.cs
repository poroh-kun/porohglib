﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Resources.Graphics
{
    public class Sprite : IpImage
    {
        public Texture2D Texture { get; private set; }
        public Rectangle TileInTexture { get; private set; }
        public Vector2 TileCenter { get; private set; }

        public TimeSpan Duration { get { return TimeSpan.Zero; } }

        public string Group { get; private set; }
        public string Name { get; private set; }

        public Sprite this[int i] { get { return this; } }

        public int Count { get { return 1; } }

        private Sprite(Texture2D texture, Rectangle tileInTexture, Vector2 tileCenter, string group, string name)
        {
            Texture = texture;
            TileInTexture = tileInTexture;
            TileCenter = tileCenter;
            Group = group;
            Name = name;
        }

        public Sprite(Texture2D texture, string group, XElement xel)
            : this(texture,
            new Rectangle(
                int.Parse(xel.Attribute("x").Value),
                int.Parse(xel.Attribute("y").Value),
                int.Parse(xel.Attribute("w").Value),
                int.Parse(xel.Attribute("h").Value)),
            new Vector2(
                int.Parse(xel.Attribute("cx").Value),
                int.Parse(xel.Attribute("cy").Value)),
            group,
            xel.Attribute("name").Value)
        {

        }

    }
}
