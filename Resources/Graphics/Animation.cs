﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Resources.Graphics
{
    public class Animation : IpImage
    {
        private Sprite[] Frames;
        public TimeSpan Duration { get; private set; }

        public string Group { get; private set; }
        public string Name { get; private set; }

        public Sprite this[int i]
        {
            get { return Frames[i % Count]; }
        }

        public int Count
        {
            get { return Frames.Length; }
        }

        public Animation(List<Sprite> images, int duration, string group, string name)
        {
            Frames = images.ToArray<Sprite>();
            Duration = new TimeSpan(0, 0, 0, 0, duration);
            Group = group;
            Name = name;
        }

    }
}
