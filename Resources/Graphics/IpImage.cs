﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Resources.Graphics
{
    public interface IpImage
    {
        Sprite this[int i] { get; }
        int Count { get; }
        TimeSpan Duration { get; }
        string Group { get; }
        string Name { get; }
    }
}
