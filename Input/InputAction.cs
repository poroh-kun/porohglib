﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PorohGLib.Input
{
    public class InputAction
    {
        private Keys KeyboardKey { get; set; }
        private MouseButtons MouseKey { get; set; }
        public ButtonType ButtonType { get; private set; }
        public event InputButtonEvent KeyPress;
        public event InputButtonEvent KeyDown;
        public event InputButtonEvent KeyUp;

        private bool WasPressed;

        public InputAction(MouseButtons key)
        {
            MouseKey = key;
            ButtonType = Input.ButtonType.Mouse;
            WasPressed = false;
        }

        public InputAction(MouseButtons key, InputButtonEvent onKeyPress, InputButtonEvent onKeyDown, InputButtonEvent onKeyUp)
            : this(key)
        {
            KeyPress = onKeyPress;
            KeyDown = onKeyDown;
            KeyUp = onKeyUp;
        }

        public InputAction(Keys key)
        {
            KeyboardKey = key;
            ButtonType = Input.ButtonType.Keyboard;
            WasPressed = false;
        }

        public InputAction(Keys key, InputButtonEvent onKeyPress, InputButtonEvent onKeyDown, InputButtonEvent onKeyUp)
            :this(key)
        {
            KeyPress = onKeyPress;
            KeyDown = onKeyDown;
            KeyUp = onKeyUp;
        }

        public void SetButton(Keys key)
        {
            KeyboardKey = key;
            ButtonType = Input.ButtonType.Keyboard;
        }

        public void SetButton(MouseButtons key)
        {
            MouseKey = key;
            ButtonType = Input.ButtonType.Mouse;
        }

        public void Update(KeyboardState kState, MouseState mState)
        {
            bool isKeyDown = false;
            if (ButtonType == Input.ButtonType.Keyboard)
            {
                isKeyDown = kState.IsKeyDown(KeyboardKey);
            }
            else
            {
                switch (MouseKey)
                {
                    case MouseButtons.Left:
                        if (mState.LeftButton == ButtonState.Pressed) isKeyDown = true;
                        break;
                    case MouseButtons.Middle:
                        if (mState.MiddleButton == ButtonState.Pressed) isKeyDown = true;
                        break;
                    case MouseButtons.Right:
                        if (mState.RightButton == ButtonState.Pressed) isKeyDown = true;
                        break;
                    case MouseButtons.XButton1:
                        if (mState.XButton1 == ButtonState.Pressed) isKeyDown = true;
                        break;
                    case MouseButtons.XButton2:
                        if (mState.XButton2 == ButtonState.Pressed) isKeyDown = true;
                        break;
                }
            }
            if (isKeyDown)
            {
                if (KeyDown != null) KeyDown(new Vector2(mState.X, mState.Y));
                if (!WasPressed)
                {
                    WasPressed = true;
                    if (KeyPress != null) KeyPress(new Vector2(mState.X, mState.Y));
                }
            }
            else
                if (WasPressed)
                {
                    WasPressed = false;
                    if (KeyUp != null) KeyUp(new Vector2(mState.X, mState.Y));
                }
        }
    }
}
