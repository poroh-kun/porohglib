﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorohGLib.Input
{
    public enum MouseButtons
    {
        Left,
        Middle,
        Right,
        XButton1,
        XButton2
    }
}
