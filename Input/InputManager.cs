﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace PorohGLib.Input
{
    public class InputManager
    {
        public Dictionary<string, InputAction> Actions { get; private set; }
        public Vector2 MousePosition { get; private set; }
        public event InputButtonEvent MouseMove;

        public InputManager()
        {
            Actions = new Dictionary<string, InputAction>();
        }

        public void AddAction(string name, InputAction action)
        {
            if (!Actions.ContainsKey(name))
                Actions.Add(name, action);
            else
                Actions[name] = action;
        }

        public void AddAction(string name, Keys key,
            InputButtonEvent keyPress = null,
            InputButtonEvent keyDown = null,
            InputButtonEvent keyUp = null)
        {
            AddAction(name, new InputAction(key));
            Actions[name].KeyPress += keyPress;
            Actions[name].KeyDown += keyDown;
            Actions[name].KeyUp += keyUp;
        }

        public void AddAction(string name, MouseButtons key,
            InputButtonEvent keyPress = null,
            InputButtonEvent keyDown = null,
            InputButtonEvent keyUp = null)
        {
            AddAction(name, new InputAction(key));
            Actions[name].KeyPress += keyPress;
            Actions[name].KeyDown += keyDown;
            Actions[name].KeyUp += keyUp;
        }

        public void Update()
        {
            KeyboardState kState = Keyboard.GetState();
            MouseState mState = Mouse.GetState();
            Vector2 newMousePosition = new Vector2(mState.X, mState.Y);
            if (newMousePosition != MousePosition)
            {
                MousePosition = newMousePosition;
                if (MouseMove != null)
                    MouseMove(MousePosition);
            }
            foreach (InputAction button in Actions.Values)
                button.Update(kState, mState);
        }

    }
}
