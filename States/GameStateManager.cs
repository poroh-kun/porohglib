﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PorohGLib;
using PorohGLib.Input;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;

namespace PorohGLib.States
{
    public class GameStateManager
    {
        public event ExitEvent ToExit;

        public GraphicsDevice GraphicsDevice { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }
        public ResourseManager Resourses { get; private set; }

        public TPS FPS { get; set; }
        public TPS UPS { get; set; }

        private Stack<GameState> States;

        public GameState CurrentState
        {
            get
            {
                if (States.Count > 0)
                    return States.Peek();
                else
                    return null;
            }
        }

        public GameStateManager(GraphicsDevice graphicsDevice, SpriteBatch spriteBath, ResourseManager resourses)
        {
            GraphicsDevice = graphicsDevice;
            SpriteBatch = spriteBath;
            Resourses = resourses;
            States = new Stack<GameState>();
            FPS = new TPS();
            UPS = new TPS();
        }

        public GameState InsertState(GameState state)
        {
            state.SetPrevState(CurrentState);
            state.Initialize(this, Resourses, GraphicsDevice, SpriteBatch);
            state.ToExit += ToExit;
            States.Push(state);
            return state;
        }

        public void RemoveState()
        {
            States.Pop();
        }

        public bool Update(GameTime gameTime)
        {
            UPS.Tick(gameTime.TotalGameTime);
            if (CurrentState != null)
            {
                CurrentState.Update(gameTime);
                return true;
            }
            return false;
        }

        public bool Draw(GameTime gameTime)
        {
            FPS.Tick(gameTime.TotalGameTime);
            if (CurrentState != null)
            {
                CurrentState.Draw(gameTime);
                return true;
            }
            return false;
        }
    }
}
