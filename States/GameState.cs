﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PorohGLib;
using PorohGLib.Input;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;

namespace PorohGLib.States
{
    public abstract class GameState
    {
        public event ExitEvent ToExit;
        public bool TransparentInput { get; set; }
        public bool TransparentUpdate { get; set; }
        public bool TransparentDraw { get; set; }
        public bool TopState { get { return StateManager.CurrentState == this; } }

        public GraphicsDevice GraphicsDevice { get; private set; }
        public SpriteBatch SpriteBatch { get; private set; }
        public BasicEffect BasicEffect { get; private set; }
        public InputManager InputManager { get; protected set; }
        public ResourseManager Resourses { get; private set; }
        public GameStateManager StateManager { get; private set; }

        protected GameState PrevievState;

        public GameState(bool transparentInput, bool transparentUpdate, bool transparentDraw)
        {
            TransparentInput = transparentInput;
            TransparentUpdate = transparentUpdate;
            TransparentDraw = transparentDraw;
            InputManager = new InputManager();
        }

        public virtual void Initialize(GameStateManager stateManager, ResourseManager resourses, GraphicsDevice graphicsDevice, SpriteBatch spriteBath)
        {
            StateManager = stateManager;
            Resourses = resourses;
            GraphicsDevice = graphicsDevice;
            SpriteBatch = spriteBath;

            BasicEffect = new BasicEffect(GraphicsDevice);
            BasicEffect.VertexColorEnabled = true;
            BasicEffect.Projection = Matrix.CreateOrthographicOffCenter
               (0, GraphicsDevice.Viewport.Width,               // left, right
                GraphicsDevice.Viewport.Height, 0,              // bottom, top
                0, 1);                                          // near, far plane

        }

        public void Return()
        {
            StateManager.RemoveState();
        }

        protected void Exit()
        {
            ToExit();
        }

        public void SetPrevState(GameState state)
        {
            PrevievState = state;
        }

        public virtual void UpdateInput()
        {
            if (TransparentInput && PrevievState != null)
                PrevievState.UpdateInput();
            InputManager.Update();
        }

        public virtual void Update(GameTime gameTime)
        {
            UpdateInput();
            if (TransparentUpdate && PrevievState != null)
                PrevievState.Update(gameTime);
        }

        public virtual void Draw(GameTime gameTime)
        {
            if (TransparentDraw && PrevievState != null)
                PrevievState.Draw(gameTime);
        }

        protected virtual void DrawPrimitives(GameTime gameTime)
        {
                BasicEffect.CurrentTechnique.Passes[0].Apply();
        }

        protected virtual void DrawString(string text, SpriteBatch spriteBatch, string font, Vector2 position, Color color)
        {
            spriteBatch.DrawString(Resourses.GetFont(font), text, position, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }
    }
}
