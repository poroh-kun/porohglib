﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorohGLib
{
    public class GameConfig
    {
        private Dictionary<string, ConfigParam> Values;
        private Dictionary<string, ConfigParam> Schema;

        public GameConfig(List<ConfigParam> schema, string filename = "config.ini")
        {
            Schema = new Dictionary<string, ConfigParam>();
            foreach (ConfigParam cf in schema)
                if (!Schema.ContainsKey(cf.Name))
                    Schema.Add(cf.Name, cf);
            Values = new Dictionary<string, ConfigParam>();
            LoadConfig(filename);
        }

        public void LoadConfig(string filename)
        {
            string[] configTxt = System.IO.File.ReadAllLines(filename);
            foreach (string line in configTxt)
            {
                string vline = line.Trim();
                if (vline.Contains(';'))
                    vline = vline.Substring(0, vline.IndexOf(';'));
                if (vline.Length > 0)
                {
                    string[] aline = vline.Split('=');
                    AddValue(aline[0].Trim().ToUpper(), aline[1].Trim().ToUpper());
                }
            }
        }

        private  void AddValue(string parameter, string value)
        {
            if (!Values.ContainsKey(parameter) && Schema.ContainsKey(parameter))
            {
                Values.Add(parameter, ConfigParam.Parse(parameter, value, Schema[parameter].Type, Schema[parameter].Value));
            }
        }

        public ConfigParam this[string key]
        {
            get
            {
                return Values[key];
            }
        }

        public bool ContainsKey(string key)
        {
            return Values.ContainsKey(key);
        }

    }
}
