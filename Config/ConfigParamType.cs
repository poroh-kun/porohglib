﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorohGLib
{
    public enum ConfigParamType
    {
        Bool,
        Int,
        Double,
        String,
        IntArray,
        DoubleArray,
        StringArray
    }
}
