﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorohGLib
{
    public class ConfigParam
    {
        public string Name { get; private set; }
        public ConfigParamType Type { get; private set; }
        public object Value { get; private set; }

        public ConfigParam()
        {
            Name = "";
            Type = ConfigParamType.String;
            Value = "";
        }

        public ConfigParam(string name, ConfigParamType type, object value)
        {
            Name = name;
            Type = type;
            Value = value;
        }

        public static ConfigParam Parse(string name, string value, ConfigParamType type, object defaultValue)
        {
            switch (type)
            {
                case ConfigParamType.Bool:
                    {
                        bool result = false;
                        if (Boolean.TryParse(value, out result))
                            return new ConfigParam(name, ConfigParamType.Bool, result);
                        else
                            return new ConfigParam(name, ConfigParamType.Bool, defaultValue);
                    }
                case ConfigParamType.Int:
                    {
                        int result = 0;
                        if (Int32.TryParse(value, out result))
                            return new ConfigParam(name, ConfigParamType.Int, result);
                        else
                            return new ConfigParam(name, ConfigParamType.Int, defaultValue);
                    }
                case ConfigParamType.Double:
                    {
                        double result = 0;
                        if (Double.TryParse(value, out result))
                            return new ConfigParam(name, ConfigParamType.Double, result);
                        else
                            return new ConfigParam(name, ConfigParamType.Double, defaultValue);
                    }
                case ConfigParamType.String:
                    {
                        string result = value;
                        return new ConfigParam(name, ConfigParamType.String, result);
                    }
                case ConfigParamType.IntArray:
                    {
                        char sep = ',';
                        if (value.Contains('x')) sep = 'x';
                        string[] arr = value.Split(sep);
                        int[] result = new int[arr.Length];
                        bool ok = true;
                        for (int i = 0; i < arr.Length; i++)
                        {
                            ok = ok && Int32.TryParse(arr[i].Trim(), out result[i]);
                        }
                        if (ok)
                            return new ConfigParam(name, ConfigParamType.IntArray, result);
                        else
                            return new ConfigParam(name, ConfigParamType.IntArray, defaultValue);
                    }
                case ConfigParamType.DoubleArray:
                    {
                        string[] arr = value.Split(',');
                        double[] result = new double[arr.Length];
                        bool ok = true;
                        for (int i = 0; i < arr.Length; i++)
                        {
                            ok = ok && Double.TryParse(arr[i].Trim(), out result[i]);
                        }
                        if (ok)
                            return new ConfigParam(name, ConfigParamType.DoubleArray, result);
                        else
                            return new ConfigParam(name, ConfigParamType.DoubleArray, defaultValue);
                    }
                case ConfigParamType.StringArray:
                    {
                        string[] result = value.Split(',');
                        for (int i = 0; i < result.Length; i++)
                        {
                            result[i] = result[i].Trim();
                        }
                        return new ConfigParam(name, ConfigParamType.StringArray, result);
                    }
                default:
                    return new ConfigParam();
            }
        }
    }
}
