﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Helpers
{
    public static class ColorHelper
    {
        public static Color HexToColor(String color)
        {
            if ((color.StartsWith("#")) && (color.Length == 9))
            {
                int r = int.Parse(color.Substring(1, 2), System.Globalization.NumberStyles.HexNumber);
                int g = int.Parse(color.Substring(3, 2), System.Globalization.NumberStyles.HexNumber);
                int b = int.Parse(color.Substring(5, 2), System.Globalization.NumberStyles.HexNumber);
                int a = int.Parse(color.Substring(7, 2), System.Globalization.NumberStyles.HexNumber);
                return new Color(r, g, b, a);
            }
            else
                return Color.White;
        }

        public static String ColorToHex(Color color)
        {
            return string.Concat('#',
                color.R.ToString("X").PadLeft(2, '0'),
                color.G.ToString("X").PadLeft(2, '0'),
                color.B.ToString("X").PadLeft(2, '0'),
                color.A.ToString("X").PadLeft(2, '0'));
        }
    }
}
