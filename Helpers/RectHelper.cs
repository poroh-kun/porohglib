﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;

namespace PorohGLib.Helpers
{
    public static class RectHelper
    {
        public static Rectangle GetRectangle(Vector3 position, Vector3 center, Vector3 size)
        {
            return new Rectangle((int)(position.X - center.X), (int)(position.Z + center.Z), (int)size.X, (int)size.Z);
        }

        public static bool RectIntrsect(Rectangle rect1, Rectangle rect2)
        {
            return rect1.Intersects(rect2) || rect1.Contains(rect2) || rect2.Contains(rect1);
        }
    }
}
