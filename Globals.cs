﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Threading;
using System.Xml.Linq;
using System.IO;
using System.Collections;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Random = System.Random;
using PorohGLib.Resourses;
using PorohGLib.States;

namespace PorohGLib
{
    public sealed partial class PGL
    {
        public PGame Game;
        private Random _Rnd;
        private List<int> _RndItems;
        public int Rnd
        {
            get
            {
                int rnd = _Rnd.Next(int.MaxValue);
                do
                {
                    rnd = _Rnd.Next(int.MaxValue);
                }
                while (_RndItems.Contains(rnd));
                _RndItems.Add(rnd);
                return rnd;
            }
        }
        public Random Random { get { return _Rnd; } }

        public Point SS; //screen size
        public int Width { get { return SS.X; } }
        public int Height { get { return SS.Y; } }

        public SpriteFont CandaraFont { get; private set; }
        public SpriteFont SmallFont { get; private set; }

        public ResourseManager Resourses { get; private set; }
        public GameStateManager States { get; private set; }
        public GameConfig Config { get; private set; }

        public double FPS { get { return States.FPS.Ticks; } }
        public double UPS { get { return States.UPS.Ticks; } }

        private PGL()
        {
            _Rnd = new Random(DateTime.Now.TimeOfDay.Milliseconds);
            _RndItems = new List<int>();
            Resourses = new ResourseManager();
        }

        public void Initialize(PGame game)
        {
            Game=game;
            States = new GameStateManager(game.GraphicsDevice, game.spriteBatch);
        }

        internal void InitializeConfig(Dictionary<string, ConfigParam> schema, string filename = "config.ini")
        {
            Config = new GameConfig(schema, filename);
        }

        public void DrawString(string text, SpriteBatch spriteBatch, string font, Vector2 position, Color color)
        {
            spriteBatch.DrawString(GetFont(font), text, position, color, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f);
        }

        public SpriteFont GetFont(string font)
        {
            switch (font)
            {
                case "Candara": return CandaraFont;
                case "Small": return SmallFont;
                default: return CandaraFont;
            }
        }

        public override void LoadContent()
        {
            CandaraFont = Game.Content.Load<SpriteFont>(@"Fonts/Candara");
            SmallFont = Game.Content.Load<SpriteFont>(@"Fonts/font");


        }

        public virtual void Update(GameTime gameTime)
        {
            States.Update(gameTime);
        }

        public virtual void Draw(GameTime gameTime)
        {
            States.Draw(gameTime);
        }
    }

}
