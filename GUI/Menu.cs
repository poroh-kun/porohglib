﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using PorohGLib.Resources.Graphics;
using PorohGLib.Input;

namespace PorohGLib.GUI
{
    public class Menu
    {
        public List<MenuItem> Items { get; set; }
        public Point Position { get; set; }
        MenuItem EscapeItem { get; set; }

        public Menu(InputManager inputManager, Point position)
        {
            Items = new List<MenuItem>();
            Position = position;
            inputManager.AddAction("menu_mouseclick", new InputAction(MouseButtons.Left));
            inputManager.AddAction("menu_up", new InputAction(Keys.Up));
            inputManager.AddAction("menu_down", new InputAction(Keys.Down));
            inputManager.AddAction("menu_enter", new InputAction(Keys.Enter));
            inputManager.AddAction("menu_escape", new InputAction(Keys.Escape));
            inputManager.Actions["menu_mouseclick"].KeyDown += new InputButtonEvent(Menu_mousedown);
            inputManager.Actions["menu_mouseclick"].KeyUp += new InputButtonEvent(Menu_mouseup);
            inputManager.Actions["menu_up"].KeyPress += new InputButtonEvent(Menu_up);
            inputManager.Actions["menu_down"].KeyPress += new InputButtonEvent(Menu_down);
            inputManager.Actions["menu_enter"].KeyPress += new InputButtonEvent(Menu_enter);
            inputManager.Actions["menu_escape"].KeyUp += new InputButtonEvent(Menu_escape);
        }

        public MenuItem AddItem(MenuItem menuItem, bool escapeItem)
        {
            if (Items.Count > 0)
                menuItem.Position =
                    new Rectangle(Position.X, Items.Last().Position.Bottom, menuItem.Size.X, menuItem.Size.Y);
            else
                menuItem.Position =
                    new Rectangle(Position.X, Position.Y, menuItem.Size.X, menuItem.Size.Y);

            Items.Add(menuItem);
            if (escapeItem) EscapeItem = menuItem;
            return menuItem;
        }

        void Menu_mousedown(Vector2 position)
        {
            foreach (MenuItem item in Items)
            {
                if (item.Position.Contains((int)position.X, (int)position.Y))
                {
                    item.State = MenuItemState.Pressed;
                }
            }
        }

        void Menu_mouseup(Vector2 position)
        {
            foreach (MenuItem item in Items)
            {
                if (item.Position.Contains((int)position.X, (int)position.Y) && item.State == MenuItemState.Pressed)
                {
                    item.Release();
                }
            }
        }

        void Menu_up(Vector2 position)
        {
            
        }

        void Menu_down(Vector2 position)
        {
            
        }

        void Menu_enter(Vector2 position)
        {
            
        }

        void Menu_escape(Vector2 position)
        {
            if (EscapeItem != null)
                EscapeItem.Release();
        }

        public void Update(Point mousePos)
        {
            MenuItem wasSelected = null;
            bool newSelect = false;
            foreach (MenuItem item in Items)
            {
                if (item.State == MenuItemState.Selected)
                    wasSelected = item;
                if (item.Position.Contains(mousePos))
                {
                    newSelect = true;
                    item.State = MenuItemState.Selected;
                }
                else
                {
                    item.State = MenuItemState.None;
                }
            }
            //if (!newSelect && wasSelected!=null)
            //{
            //    wasSelected.State = MenuItemState.Selected;
            //}
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < Items.Count; i++)
            {
                //Items[i].Draw(spriteBatch);
            }
        }
    }
}
