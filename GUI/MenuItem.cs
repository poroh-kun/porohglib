﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.Input;

namespace PorohGLib.GUI
{
    public delegate void MenuItemEvent();

    public class MenuItem
    {
        public event MenuItemEvent ItemPressed;

        public string Text { get; set; }
        public object Tag { get; set; }
        public Sprite Picture0 { get; set; }
        public Sprite Picture1 { get; set; }
        public Sprite Picture2 { get; set; }
        public MenuItemState State { get; set; }
        public Point Size { get; set; }
        public Rectangle Position { get; set; }

        public List<MenuItem> Items { get; set; }

        public MenuItem(ResourseManager rm, string text, string picture)
        {
            Text = text;
            Picture0 = (Sprite)rm.GetImage("menu", picture + "0");
            Picture1 = (Sprite)rm.GetImage("menu", picture + "1");
            Picture2 = (Sprite)rm.GetImage("menu", picture + "2");
            State = MenuItemState.None;
            Size = new Point(Picture0.TileInTexture.Width, Picture0.TileInTexture.Height);
            
            Items = new List<MenuItem>();
        }

        public void Release()
        {
            ItemPressed();
        }

        //public void Draw(SpriteBatch spriteBatch)
        //{
        //    Point position = new Point(Position.Left, Position.Top);
        //    switch (State)
        //    {
        //        case MenuItemState.None:
        //            Picture0.Prepare(0,position.X,position.Y, SpriteEffects.None, Color.White).Draw(spriteBatch);
        //            break;
        //        case MenuItemState.Selected:
        //            Picture1.Prepare(0, position.X, position.Y, SpriteEffects.None, Color.White).Draw(spriteBatch);
        //            break;
        //        case MenuItemState.Pressed:
        //            Picture2.Prepare(0, position.X, position.Y, SpriteEffects.None, Color.White).Draw(spriteBatch);
        //            break;
        //    }
        //}
    }

    public enum MenuItemState
    {
        None,
        Selected,
        Pressed
    }
}
