﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PorohGLib.Core.Objects;

namespace PorohGLib.Core
{
    public interface IMotor
    {
        int Id { get; }
        GameObject Object { get; }
        void SendCommand(string command, params object[] args);
    }
}
