﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Core.Modules;
using PorohGLib.Core.Modules.Collisions;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Objects
{
    public class GameMap : GameObject
    {
        public override string Label { get { return "GameMap"; } }
        private List<MapLayer> Layers;
        public override IEnumerable<GameObject> Childs
        {
            get
            {
                foreach (MapLayer layer in Layers)
                    foreach (GameObject entity in layer.Childs)
                        yield return entity;
            }
        }
        private List<CollisionBox> Colliders;

        public ResourseManager Resourses;

        public IMotor Player { get; private set; }
        public Camera Camera { get; private set; }

        public GameMap(Vector2 position, ResourseManager rm)
            : base(position, null, null)
        {
            Resourses = rm;
            Map = this;
            Parent = this;

            Layers = new List<MapLayer>();
            Colliders = new List<CollisionBox>();
        }

        public GameMap(XElement xel, ResourseManager rm)
            : base(xel, null, null)
        {
            Resourses = rm;
            Map = this;
            Parent = this;

            Layers = new List<MapLayer>();
            Colliders = new List<CollisionBox>();

            foreach (XElement xl in xel.Element("Layers").Elements("MapLayer"))
            {
                Layers.Add(new MapLayer(xl, this));
            }
            SortLayers();

        }

        public void AddLayer(MapLayer layer)
        {
            Layers.Add(layer);
        }

        public IEnumerable<MapLayer> GetLayers()
        {
            foreach (MapLayer layer in Layers)
                yield return layer;
        }

        public void RemoveLayer(MapLayer layer)
        {
            if (Layers.Contains(layer))
                Layers.Remove(layer);
        }

        public void SetPlayer(IMotor player)
        {
            Player = player;
        }

        public void SetCamera(Camera cam)
        {
            Camera = cam;
        }
        public static GameMap LoadMap(string filename, ResourseManager rm)
        {
            GameMap map = new GameMap(XElement.Load(filename), rm);
            map.ThrowLinks();
            map.DropIdWithChilds();
            IdPool.ClearPool();
            return map;
        }

        public override void ThrowLinks()
        {
            Player = (IMotor)IdPool.GetEntityById(Links["player"]);
            Camera = (Camera)IdPool.GetEntityById(Links["camera"]);
            base.ThrowLinks();
        }

        public void Save(string filename)
        {
            RestoreLinksIdsWithChilds();
            ToXml().Save(filename);
        }

        protected virtual void SortLayers()
        {
            Layers.Sort(delegate(MapLayer layer1, MapLayer layer2)
            {
                return layer1.Level.CompareTo(layer2.Level);
            });
        }

        public void Draw(SpriteBatch spriteBath, GameTime gameTime)
        {
            Camera.Draw(spriteBath, gameTime, this);
        }

        public void Draw(GraphicsDevice graphicsDevice, GameTime gameTime)
        {
            Camera.Draw(graphicsDevice, gameTime, this);
        }

        public List<CollisionBox> GetTerrain(Vector3 position, int distanse = 256)
        {
            List<CollisionBox> result = new List<CollisionBox>();
            //foreach (Block block in Blocks)
            //{
            //    Vector3 dist = block.Position - position;
            //    if (Math.Abs(dist.X) <= distanse && Math.Abs(dist.Z) <= distanse)
            //        result.Add(block);
            //}
            return result;
        }

        public IEnumerable<CollisionBox> GetColliders(CollisionBox except)
        {
            return Colliders.FindAll(box => box.Enabled && box != except);
        }

        public void AddCollider(CollisionBox box)
        {
            Colliders.Add(box);
        }

        public override XElement ToXml()
        {
            XElement xl = new XElement("Layers");
            XElement xlink = new XElement("Links",
                new XElement("Link",
                    new XAttribute("label", "player"),
                    new XAttribute("id", Player.Id)),
                new XElement("Link",
                    new XAttribute("label", "camera"),
                    new XAttribute("id", Camera.Id)));
            XElement xel = new XElement(Label, xl, xlink,
                new XAttribute("x", Position.X.ToString(CultureInfo.InvariantCulture.NumberFormat)),
                new XAttribute("y", Position.Y.ToString(CultureInfo.InvariantCulture.NumberFormat)));
            foreach (MapLayer layer in Layers)
            {
                xl.Add(layer.ToXml());
            }
            return xel;
        }
    }
}
