﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Core.Modules;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.Helpers;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Objects
{
    public class GameObject : GameEntity
    {
        public override string Label { get { return "GameObject"; } }

        public GameMap Map;
        public virtual GameObject Parent { get; protected set; }
        
        protected List<GameObject> GObjects = new List<GameObject>();
        public virtual IEnumerable<GameObject> Childs { get { foreach (GameObject gobj in GObjects) yield return gobj; } }
        protected List<GameModule> _Modules = new List<GameModule>();
        public virtual IEnumerable<GameModule> Modules { get { foreach (GameModule module in _Modules) yield return module; } }

        private Vector2 _Position { get; set; }
        public virtual Vector2 Position { get { return _Position; } protected set { _Position = value; } }
        public virtual Vector2 GlobalPosition { get { return this == Parent ? Position : (Parent.Position + Position); } }

        public event EventHandler<MovingEventArgs> Moved;

        public GameObject(Vector2 position, GameMap map, GameObject parent)
        {
            Map = map;
            Parent = parent;
            _Position = position;
        }

        public GameObject(XElement xel, GameMap map, GameObject parent)
        {
            if (xel.Attributes("id").Count() > 0)
            {
                _Id = int.Parse(xel.Attribute("id").Value);
                IdPool.ManualAddId(_Id, this);
            }
            Map = map;
            Parent = parent;
            _Position = new Vector2(
                float.Parse(xel.Attribute("x").Value, CultureInfo.InvariantCulture.NumberFormat),
                float.Parse(xel.Attribute("y").Value, CultureInfo.InvariantCulture.NumberFormat));
            if (xel.Elements("Modules").Count() > 0)
                foreach (XElement xl in xel.Element("Modules").Elements())
                {
                    _Modules.Add(Factory.GetModule(xl, this));
                }
            if (xel.Elements("Links").Count() > 0)
                foreach (XElement xl in xel.Element("Links").Elements())
                {
                    Links.Add(xl.Attribute("label").Value, int.Parse(xl.Attribute("id").Value));
                }
            if (xel.Elements("Childs").Count() > 0)
                foreach (XElement xl in xel.Element("Childs").Elements())
                {
                    GObjects.Add(Factory.GetObject(xl, map, this));
                }
        }

        public void AddModule(GameModule module)
        {
            _Modules.Add(module);
        }

        public void RemoveModule(GameModule module)
        {
            if (_Modules.Contains(module))
                _Modules.Remove(module);
        }

        public void AddChild(GameObject child)
        {
            GObjects.Add(child);
        }

        public void RemoveChild(GameObject child)
        {
            if (GObjects.Contains(child))
                GObjects.Remove(child);
        }

        public override void ThrowLinks()
        {
            foreach (GameObject go in Childs)
                go.ThrowLinks();

            foreach (GameModule gm in Modules)
                gm.ThrowLinks();
            base.ThrowLinks();
        }

        public void DropIdWithChilds()
        {
            DropId();
            foreach (GameModule gm in Modules)
                gm.DropId();
            foreach (GameObject go in Childs)
                go.DropIdWithChilds();
        }

        public void RestoreLinksIdsWithChilds()
        {
            RestoreLinksIds();
            foreach (GameModule gm in Modules)
                gm.RestoreLinksIds();
            foreach (GameObject go in Childs)
                go.RestoreLinksIdsWithChilds();
        }

        public GameModule GetModule(string label)
        {
            return Modules.First(m => m.Label == label);
        }

        public virtual void PreUpdate(GameTime gameTime)
        {
            foreach (GameModule gmod in Modules)
                gmod.PreUpdate(gameTime);
            foreach (GameObject gobj in Childs)
                gobj.PreUpdate(gameTime);
        }

        public virtual void Update(GameTime gameTime)
        {
            foreach (GameModule gmod in Modules)
                gmod.Update(gameTime);
            foreach (GameObject gobj in Childs)
                gobj.Update(gameTime);
        }

        public virtual void PostUpdate(GameTime gameTime)
        {
            foreach (GameModule gmod in Modules)
                gmod.PostUpdate(gameTime);
            foreach (GameObject gobj in Childs)
                gobj.PostUpdate(gameTime);
        }

        public virtual bool IsVisibleAt(Rectangle fieldOfView)
        {
            return true;
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, Rectangle fieldOfView, Vector2 viewPoint)
        {
            foreach (GameModule gmod in Modules)
                if (gmod.IsVisibleAt(fieldOfView))
                    gmod.Draw(spriteBatch, gameTime, viewPoint);
            foreach (GameObject gobj in Childs)
                if (gobj.IsVisibleAt(fieldOfView))
                    gobj.Draw(spriteBatch, gameTime, fieldOfView, viewPoint);
        }

        public virtual void Draw(GraphicsDevice graphicsDevice, GameTime gameTime, Rectangle fieldOfView, Vector2 viewPoint)
        {
            foreach (GameModule gmod in Modules)
                if (gmod.IsVisibleAt(fieldOfView))
                    gmod.Draw(graphicsDevice, gameTime, viewPoint);
            foreach (GameObject gobj in Childs)
                if (gobj.IsVisibleAt(fieldOfView))
                    gobj.Draw(graphicsDevice, gameTime, fieldOfView, viewPoint);
        }

        public virtual void Move(Vector2 direction)
        {
            if (direction != Vector2.Zero)
            {
                Vector2 oldPosition = Position + direction;
                    Position += direction;
                MovingEventArgs mea = new MovingEventArgs(oldPosition, Position);
                if (Moved != null)
                    Moved(this, mea);
            }
        }

        public void MoveTo(Vector2 position)
        {
            Move(position - Position);
        }

        public GameObject Clone()
        {
            XElement xel = ToXml();
            if (xel.Attributes("id").Count() > 0)
                xel.Attribute("id").Remove();
            return Factory.GetObject(xel, Map, Parent);
        }

        public virtual XElement ToXml()
        {
            XElement xel = new XElement(Label,
                new XAttribute("x", Position.X.ToString(CultureInfo.InvariantCulture.NumberFormat)),
                new XAttribute("y", Position.Y.ToString(CultureInfo.InvariantCulture.NumberFormat)));

            int modules = 0;
            XElement xlm = new XElement("Modules");
            foreach (GameModule gm in Modules)
            {
                xlm.Add(gm.ToXml());
                modules++;
            }
            if (modules > 0)
                xel.Add(xlm);

            int childs = 0;
            XElement xl = new XElement("Childs");
            foreach (GameObject go in Childs)
            {
                xl.Add(go.ToXml());
                childs++;
            }
            if (childs > 0)
                xel.Add(xl);

            if (_Id != -1)
                xel.Add(new XAttribute("id", Id));
            return xel;
        }

    }
}
