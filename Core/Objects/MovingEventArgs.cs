﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using PorohGLib;
using PorohGLib.Helpers;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace PorohGLib.Core.Objects
{
    [Serializable]
    [ComVisible(true)]
    public class MovingEventArgs : EventArgs
    {
        public Vector2 OldPosition { get; private set; }
        public Vector2 NewPosition { get; private set; }

        public MovingEventArgs(Vector2 oldPosition, Vector2 newPosition)
        {
            OldPosition = oldPosition;
            NewPosition = newPosition;
        }
    }
}
