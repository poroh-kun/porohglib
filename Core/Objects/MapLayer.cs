﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PorohGLib.Resources.Graphics;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Objects
{
    public class MapLayer : GameObject
    {
        public override string Label { get { return "MapLayer"; } }
        public override GameObject Parent { get { return Map; } }

        public string Name { get; private set; }
        public int Level { get; private set; }

        public MapLayer(Vector2 position, string name, GameMap map)
            : base(position, map, map)
        {
            Name = name;
        }

        public MapLayer(XElement xel, GameMap map)
            :base(xel,map,map)
        {
            Name = xel.Attribute("name").Value.ToString();
            Level = int.Parse(xel.Attribute("level").Value);
        }

        public void AddObject(GameObject item)
        {
            GObjects.Add(item);
        }

        public void Draw()
        {

        }

        public override XElement ToXml()
        {
            XElement xel = base.ToXml();
            xel.Add(new XAttribute("name", Name));
            xel.Add(new XAttribute("level", Level));
            return xel;
        }
    }
}
