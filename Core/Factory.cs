﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Core.Modules;
using PorohGLib.Core.Modules.Collisions;
using PorohGLib.Core.Objects;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core
{
    public delegate GameObject FactoryObjectHandler(XElement xel, GameMap map, GameObject parent);
    public delegate GameModule FactoryModuleHandler(XElement xel, GameObject obj);

    public static class Factory
    {
        public static event FactoryObjectHandler ObjectExtended;
        public static event FactoryModuleHandler ModuleExtended;

        public static GameObject GetObject(XElement xel, GameMap map, GameObject parent)
        {
            string name = xel.Name.ToString();
            switch (name)
            {
                case "GameObject":
                    return new GameObject(xel, map, parent);
            }
            if (ObjectExtended != null) return ObjectExtended(xel, map, parent);
            
            throw new ArgumentException(name + " не является допустимым объетом.");
        }

        public static GameModule GetModule(XElement xel, GameObject obj)
        {
            string name = xel.Name.ToString();
            switch (name)
            {
                case "GameModule":
                    return new GameModule(xel, obj);
                case "CollisionBox":
                    return new CollisionBox(xel, obj);
                case "GraphicsController":
                    return new GraphicsController(xel, obj);
                case "Camera":
                    return new Camera(xel, obj);
                case "SimpleMotor":
                    return new SimpleMotor(xel, obj);
                case "CameraMotor":
                    return new CameraMotor(xel, obj);
            }
            if (ModuleExtended != null) return ModuleExtended(xel, obj);

            throw new ArgumentException(name + " не является допустимым модулем.");
        }
    }
}
