﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Core.Objects;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.Helpers;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core
{
    public abstract class GameEntity
    {
        public virtual string Label { get { return "GameEntity"; } }
        internal int _Id { get; set; }
        public int Id { get { if (_Id == -1) GenerateId(); return _Id; } }
        protected Dictionary<string, int> Links = new Dictionary<string, int>();
        private List<GameEntity> LinkedEntities = new List<GameEntity>();

        public GameEntity()
        {
            _Id = -1;
        }

        public virtual void ThrowLinks()
        {
            foreach (int id in Links.Values)
                LinkedEntities.Add(IdPool.GetEntityById(id));
            Links.Clear();
        }

        public void DropId()
        {
            _Id = -1;
        }

        public void GenerateId()
        {
            _Id = IdPool.GetNewId(this);
        }

        public virtual void RestoreLinksIds()
        {
            foreach (GameEntity entity in LinkedEntities)
            {
                entity.GenerateId();
            }
        }
    }
}
