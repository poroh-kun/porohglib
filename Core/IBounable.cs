﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Core.Objects;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using System.Xml.Linq;
using System.Globalization;
using Rectangle = PorohGLib.Core.Modules.Collisions.Rectangle;

namespace PorohGLib.Core
{
    public interface IBounable
    {
        int Id { get; }
        Rectangle GetRectangle();
    }
}
