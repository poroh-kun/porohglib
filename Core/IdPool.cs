﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PorohGLib.Core.Objects;
using PorohGLib.Core.Modules;

namespace PorohGLib.Core
{
    public static class IdPool
    {

        private static Random Rnd = new Random();
        private static Dictionary<int, GameEntity> EntitiesIDs = new Dictionary<int, GameEntity>();

        public static int GetNewId(GameEntity ge)
        {
            int id;
            do
            {
                id = Rnd.Next(0, int.MaxValue);
            } while (EntitiesIDs.ContainsKey(id));
            EntitiesIDs.Add(id, ge);
            return id;
        }

        public static void ManualAddId(int id, GameEntity ge)
        {
            EntitiesIDs.Add(id, ge);
        }

        public static GameEntity GetEntityById(int id)
        {
            return EntitiesIDs[id];
        }

        public static void ClearPool()
        {
            EntitiesIDs.Clear();
        }
    }
}
