﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorohGLib.Core.Modules.Collisions
{
    public enum DirectionY
    {
        Up = -1,
        None = 0,
        Down = 1
    }
}
