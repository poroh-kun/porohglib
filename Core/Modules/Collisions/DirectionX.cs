﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PorohGLib.Core.Modules.Collisions
{
    public enum DirectionX
    {
        Left = -1,
        None = 0,
        Right = 1
    }
}
