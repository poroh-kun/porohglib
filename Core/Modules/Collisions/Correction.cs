﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PorohGLib.Core.Modules.Collisions
{
    public struct Correction
    {
        public float Horizontal;
        public DirectionX HorizontalDirection;
        public float Vertical;
        public DirectionY VerticalDirection;

        public Vector2 GetCorrectionVector()
        {
            return new Vector2(
                Horizontal * (int)HorizontalDirection,
                Vertical * (int)VerticalDirection
                );
        }

        public bool IsEmpty { get { return HorizontalDirection == DirectionX.None && VerticalDirection == DirectionY.None; } }

        public static Correction Empty
        {
            get
            {
                return new Correction()
                {
                    Horizontal = 0,
                    Vertical = 0,
                    HorizontalDirection = DirectionX.None,
                    VerticalDirection = DirectionY.None
                };
            }
        }
    }
}
