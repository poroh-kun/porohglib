﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Core.Objects;
using Rectangle = PorohGLib.Core.Modules.Collisions.Rectangle;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Modules.Collisions
{
    public class CollisionBox:GameModule, IBounable
    {
        public override string Label { get { return "CollisionBox"; } }

        private Vector2 LastPosition = Vector2.Zero;
        private Rectangle _Bounds;
        public Rectangle Bounds { get { return _Bounds; } private set { _Bounds = value; } }

        public bool Enabled { get; set; }
        private bool _Correctable;
        public bool Correctable { get { return Enabled && _Correctable; } set { _Correctable = value; } }

        private IBounable Boundable;

        private List<CollisionBox> Colliders = new List<CollisionBox>();
        private List<Correction> Corrections = new List<Correction>();
        
        public CollisionBox(Rectangle origin, GameObject obj)
            : base(obj)
        {
            Bounds = origin;
            Enabled = true;
            Object.Map.AddCollider(this);
        }

        public CollisionBox(XElement xel, GameObject obj)
            : base(xel, obj)
        {
            Bounds = new Rectangle(xel.Element("Bounds"));
            Enabled = bool.Parse(xel.Attribute("enabled").Value);
            Correctable = bool.Parse(xel.Attribute("correctable").Value);
            Object.Moved += Object_Moved;
            Object.Map.AddCollider(this);
        }

        public override void ThrowLinks()
        {
            if (Links.Count>0)
                Boundable = (IBounable)IdPool.GetEntityById(Links["boundable"]);
            base.ThrowLinks();
        }

        public Rectangle GetRectangle()
        {
            return Bounds;
        }

        public IEnumerable<CollisionBox> GetColliders()
        {
            foreach (CollisionBox collider in Colliders)
                yield return collider;
        }

        public IEnumerable<Correction> GetCorrections()
        {
            foreach (Correction correction in Corrections)
                yield return correction;
        }

        void Object_Moved(object sender, MovingEventArgs e)
        {
            if (Boundable == null)
            {
                Vector2 offset = e.NewPosition - e.OldPosition;
                _Bounds.Offset(offset);

            }
            else
                _Bounds = Boundable.GetRectangle();
            CalculateCollision();
        }

        public void CalculateCollision()
        {
            CorrectCollision(Object.Map.GetColliders(this));
        }

        private void CorrectCollision(IEnumerable<CollisionBox> boxes)
        {
            if (Correctable)
            {
                Move(GetMinimumCorrection(GetCorrectionVectors(DetectCollisions(boxes))).GetCorrectionVector());
            }
            else
            {
                foreach (CollisionBox box in DetectCollisions(boxes))
                {
                    box.CalculateCollision();
                }
            }
        }

        private IEnumerable<CollisionBox> DetectCollisions(IEnumerable<CollisionBox> boxes)
        {
            foreach (CollisionBox box in boxes)
            {
                if (Bounds.Intersects(box.Bounds))
                {
                    if (!Colliders.Contains(box))
                        Colliders.Add(box);
                    yield return box;
                }
            }
        }

        private IEnumerable<Correction> GetCorrectionVectors(IEnumerable<CollisionBox> targets)
        {
            foreach (CollisionBox target in targets)
            {
                Correction cor = new Correction();
                float left = Bounds.Right - target.Bounds.Left;
                float right = target.Bounds.Right - Bounds.Left;
                float top = Bounds.Bottom - target.Bounds.Top;
                float bottom = target.Bounds.Bottom - Bounds.Top;

                if (left <= 0 || right <= 0)
                {
                    cor.Horizontal = 0;
                    cor.HorizontalDirection = DirectionX.None;
                }
                else
                    if (left < right)
                    {
                        cor.Horizontal = left;
                        cor.HorizontalDirection = DirectionX.Left;
                    }
                    else
                    {
                        cor.Horizontal = right;
                        cor.HorizontalDirection = DirectionX.Right;
                    }

                if (top <= 0 || bottom <= 0)
                {
                    cor.Vertical = 0;
                    cor.VerticalDirection = DirectionY.None;
                }
                else
                    if (bottom < top)
                    {
                        cor.Vertical = bottom;
                        cor.VerticalDirection = DirectionY.Down;
                    }
                    else
                    {
                        cor.Vertical = top;
                        cor.VerticalDirection = DirectionY.Up;
                    }

                if (!cor.IsEmpty)
                    yield return cor;
            }
        }

        private Correction GetMinimumCorrection(IEnumerable<Correction> corrections)
        {
            float left = float.MaxValue;
            float right = float.MaxValue;
            float top = float.MaxValue;
            float bottom = float.MaxValue;
            int leftDir = 0;
            int rightDir = 0;
            int topDir = 0;
            int bottomDir = 0;
            bool needCorrection = false;

            foreach (Correction correction in corrections)
            {
                if (correction.HorizontalDirection != DirectionX.None && correction.VerticalDirection != DirectionY.None)
                {
                    switch (correction.HorizontalDirection)
                    {
                        case DirectionX.Left:
                            left = Math.Min(left, correction.Horizontal);
                            leftDir++;
                            break;
                        case DirectionX.Right:
                            right = Math.Min(right, correction.Horizontal);
                            rightDir++;
                            break;
                    }
                    switch (correction.VerticalDirection)
                    {
                        case DirectionY.Up:
                            top = Math.Min(top, correction.Vertical);
                            topDir++;
                            break;
                        case DirectionY.Down:
                            bottom = Math.Min(bottom, correction.Vertical);
                            bottomDir++;
                            break;
                    }
                    needCorrection = true;
                }
            }
            if (needCorrection)
            {
                DirectionX horDir;
                DirectionY verDir;
                float hor;
                float ver;
                int horCount = 0;
                int verCount = 0;
                if (leftDir > rightDir)
                {
                    horDir = DirectionX.Left;
                    hor = left;
                    horCount = leftDir;
                }
                else
                {
                    horDir = DirectionX.Right;
                    hor = right;
                    horCount = rightDir;
                }
                if (bottomDir > topDir)
                {
                    verDir = DirectionY.Down;
                    ver = bottom;
                    verCount = bottomDir;
                }
                else
                {
                    verDir = DirectionY.Up;
                    ver = top;
                    verCount = topDir;
                }

                if (horCount > verCount || (horCount == verCount && hor < ver))
                {
                    Correction resultCorrection = new Correction() { Horizontal = hor, HorizontalDirection = horDir, Vertical = 0f, VerticalDirection = DirectionY.None };
                    Corrections.Add(resultCorrection);
                    return resultCorrection;
                }
                else
                {
                    Correction resultCorrection = new Correction() { Horizontal = 0f, HorizontalDirection = DirectionX.None, Vertical = ver, VerticalDirection = verDir };
                    Corrections.Add(resultCorrection);
                    return resultCorrection;
                }
            }
            else
            {
                return Correction.Empty;
            }
        }

        public void Move(Vector2 direction)
        {
            Object.Move(direction);
        }

        public override void PostUpdate(GameTime gameTime)
        {
            Colliders.Clear();
            Corrections.Clear();
            if (Boundable != null)
                _Bounds = Boundable.GetRectangle();
        }

        public override XElement ToXml()
        {
            XElement xel = base.ToXml();
            xel.Add(Bounds.ToXml("Bounds"));
            xel.Add(new XAttribute("enabled", Enabled));
            xel.Add(new XAttribute("correctable", _Correctable));
            if (Boundable != null)
            {
                xel.Add(new XElement("Links", new XElement("Link", new XAttribute("label", "boundable"), new XAttribute("id", Boundable.Id))));
            }
            return xel;
        }

    }
}
