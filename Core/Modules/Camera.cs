﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib.Resources.Graphics;
using PorohGLib.Core.Objects;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Modules
{
    public class Camera:GameModule
    {
        public override string Label { get { return "Camera"; } }

        public Vector2 Size { get; private set; }
        public Vector2 ViewPoint { get; private set; }
        public Rectangle FieldOfView { get; private set; }

        public GameObject Target { get; set; }

        public Camera(Vector2 position, Vector2 size, GameObject obj)
            :base(obj)
        {
            Size = size;
            Object.Moved += object_Moved;
            object_Moved(null, new MovingEventArgs(Vector2.Zero, Object.Position));
        }

        public Camera(XElement xel, GameObject obj)
            : base(xel, obj)
        {
            Size = new Vector2(int.Parse(xel.Attribute("width").Value), int.Parse(xel.Attribute("height").Value));
            Object.Moved += object_Moved;
            object_Moved(null, new MovingEventArgs(Vector2.Zero, Object.Position));
        }

        void object_Moved(object sender, MovingEventArgs e)
        {
            ViewPoint = e.NewPosition - Size / 2;
            FieldOfView = new Rectangle((int)ViewPoint.X, (int)ViewPoint.Y, (int)Size.X, (int)Size.Y);
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, GameObject GO)
        {
            GO.Draw(spriteBatch, gameTime, FieldOfView, ViewPoint);
        }

        public void Draw(GraphicsDevice graphicsDevice, GameTime gameTime, GameObject GO)
        {
            GO.Draw(graphicsDevice, gameTime, FieldOfView, ViewPoint);
        }

        public override XElement ToXml()
        {
            XElement xel = base.ToXml();
            xel.Add(new XAttribute("width", (int)Size.X));
            xel.Add(new XAttribute("height", (int)Size.Y));
            return xel;
        }

    }
}
