﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.Helpers;
using PorohGLib.Core.Objects;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Modules
{
    public class SimpleMotor : GameModule, IMotor
    {
        public override string Label { get { return "SimpleMotor"; } }

        protected bool Rotate = false;

        protected Vector2 Impulse = Vector2.Zero;
        public float Speed { get; set; }

        protected GraphicsController GC;

        public SimpleMotor(GameObject obj)
            : base(obj)
        {
            Speed = 0f;
        }

        public SimpleMotor(XElement xel, GameObject obj)
            :base(xel, obj)
        {
            Speed = float.Parse(xel.Attribute("speed").Value, CultureInfo.InvariantCulture.NumberFormat);
        }

        public override void ThrowLinks()
        {
            if (Links.ContainsKey("graphics"))
                GC = (GraphicsController)IdPool.GetEntityById(Links["graphics"]);
            base.ThrowLinks();
        }

        public void SendCommand(string command, params object[] args)
        {
            switch (command)
            {
                case "move_left":
                    MoveLeft();
                    break;
                case "move_right":
                    MoveRight();
                    break;
                case "move_up":
                    MoveUp();
                    break;
                case "move_down":
                    MoveDown();
                    break;
            }
        }

        protected void MoveLeft()
        {
            Impulse += new Vector2(-Speed, 0);
            Rotate = true;
        }

        protected void MoveRight()
        {
            Impulse += new Vector2(Speed, 0);
            Rotate = false;
        }

        protected void MoveUp()
        {
            Impulse += new Vector2(0, -Speed);
        }

        protected void MoveDown()
        {
            Impulse += new Vector2(0, Speed);
        }

        public override void Update(GameTime gameTime)
        {

            Vector2 offset = Impulse * (float)gameTime.ElapsedGameTime.TotalMilliseconds / 1000f;

            Object.Move(offset);
            Impulse = Vector2.Zero;
            if (GC != null)
                GC.Flip = Rotate ? SpriteEffects.FlipHorizontally : SpriteEffects.None;

            base.Update(gameTime);
        }

        public override XElement ToXml()
        {
            XElement xel = base.ToXml();
            if (GC != null)
            {
                XElement xlink = new XElement("Links",
                    new XElement("Link",
                        new XAttribute("label", "graphics"),
                        new XAttribute("id", GC.Id)));
                xel.Add(xlink);
            }
            xel.Add(new XAttribute("speed", Speed.ToString(CultureInfo.InvariantCulture.NumberFormat)));
            return xel;
        }
    }
}
