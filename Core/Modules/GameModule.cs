﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Core.Objects;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.Helpers;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Modules
{
    public class GameModule : GameEntity
    {
        public override string Label { get { return "GameModule"; } }

        public GameObject Object { get; protected set; }

        public GameModule(GameObject obj)
        {
            Object = obj;
        }

        public GameModule(XElement xel, GameObject obj)
        {
            if (xel.Attributes("id").Count() > 0)
            {
                _Id = int.Parse(xel.Attribute("id").Value);
                IdPool.ManualAddId(_Id, this);
            }
            Object = obj;
            if (xel.Elements("Links").Count() > 0)
                foreach (XElement xl in xel.Element("Links").Elements())
                {
                    Links.Add(xl.Attribute("label").Value, int.Parse(xl.Attribute("id").Value));
                }
        }

        public virtual void PreUpdate(GameTime gameTime) { }

        public virtual void Update(GameTime gameTime) { }

        public virtual void PostUpdate(GameTime gameTime) { }

        public virtual bool IsVisibleAt(Rectangle fieldOfView)
        {
            return false;
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, Vector2 viewPoint) { }

        public virtual void Draw(GraphicsDevice graphicsDevice, GameTime gameTime, Vector2 viewPoint) { }

        public GameModule Clone()
        {
            XElement xel = ToXml();
            if (xel.Attributes("id").Count() > 0)
                xel.Attribute("id").Remove();
            return Factory.GetModule(xel, Object);
        }

        public virtual XElement ToXml()
        {
            XElement xel = new XElement(Label);
            if (_Id != -1)
                xel.Add(new XAttribute("id", Id));
            return xel;
        }

    }
}
