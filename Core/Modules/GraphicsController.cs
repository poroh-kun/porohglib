﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PorohGLib.Core.Objects;
using Rectangle = PorohGLib.Core.Modules.Collisions.Rectangle;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.Helpers;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Modules
{
    public class GraphicsController : GameModule,IBounable
    {
        public override string Label { get { return "GraphicsController"; } }

        public Dictionary<string, IpImage> States { get; private set; }
        public string State { get; private set; }
        public string NextState { get; private set; }
        public int Frame { get; private set; }
        public bool Loop { get; private set; }
        public TimeSpan Duration { get { return CurrentState.Duration; } }

        private IpImage CurrentState { get { return States[State]; } }
        private Sprite CurrentSprite { get { return CurrentState[Frame]; } }

        private float _Order { get; set; }
        public float Order
        {
            get
            { return _Order; }
            set
            {
                _Order = value > 1000f ? 1000f : (value < -1000f ? -1000f : value);
                RealOrder = 0.5f - _Order / 2000f;
            }
        }
        public float RealOrder { get; private set; }
        public SpriteEffects Flip { get; set; }
        public Vector2 Position { get { return Object.Position; } }
        public Rectangle DestinationRectangle { get; private set; }
        public Color Color { get; set; }

        private TimeSpan PreviousTime;

        public GraphicsController(List<string[]> images, ResourseManager rm, GameObject obj)
            : base(obj)
        {
            States = new Dictionary<string, IpImage>();
            foreach (string[] image in images)
                States.Add(image[0], rm.GetImage(image[1], image[2]));
            State = images[0][0];
            NextState = State;
            Frame = 0;
            Loop = true;

            //Order = 0;
            Flip = SpriteEffects.None;
            Color = Color.White;
        }

        public GraphicsController(XElement xel, GameObject obj)
            : base(xel, obj)
        {
            States = new Dictionary<string, IpImage>();
            foreach (XElement xState in xel.Elements("State"))
                States.Add(
                    xState.Attribute("key").Value,
                    Object.Map.Resourses.GetImage(
                        xState.Attribute("group").Value,
                        xState.Attribute("name").Value
                        )
                    );
            State = xel.Attribute("state").Value;
            NextState = xel.Attribute("next_state").Value;
            Frame = int.Parse(xel.Attribute("frame").Value);
            Loop = bool.Parse(xel.Attribute("loop").Value);

            Flip = (SpriteEffects)Enum.Parse(typeof(SpriteEffects), xel.Attribute("flip").Value);
            Color = ColorHelper.HexToColor(xel.Attribute("color").Value);

            Object.Moved += Object_Moved;
            Object_Moved(null, new MovingEventArgs(Vector2.Zero, Object.Position));
        }

        public Rectangle GetRectangle()
        {
            return DestinationRectangle;
        }

        void Object_Moved(object sender, MovingEventArgs e)
        {
            Vector2 rectPos = e.NewPosition - CurrentSprite.TileCenter;
            Microsoft.Xna.Framework.Rectangle size = CurrentSprite.TileInTexture;
            DestinationRectangle = new Rectangle(rectPos.X, rectPos.Y, size.Width, size.Height);
        }

        public void SetState(string state)
        {
            State = state;
            NextState = state;
            Loop = true;
            Frame = 0;
        }

        public void SetState(string state, string nextState)
        {
            State = state;
            Frame = 0;
            SetNextState(nextState);
        }

        public void ContinueState(string state)
        {
            if (State != state)
                SetState(state);
            Loop = true;
        }

        public void ContinueState(string state, string nextState)
        {
            if (State != state)
                SetState(state, nextState);
            else
                if (NextState != nextState)
                    SetNextState(nextState);
        }

        public void SetNextState(string state)
        {
            NextState = state;
            Loop = false;
        }

        private void Iterate()
        {
            Frame++;
            if (Frame >= CurrentState.Count)
            {
                Frame = 0;
                if (!Loop)
                {
                    State = NextState;
                    Loop = true;
                }
            }
        }

        public override void PostUpdate(GameTime gameTime)
        {
            if (PreviousTime == null) PreviousTime = gameTime.TotalGameTime;

            TimeSpan nextTime = Duration != TimeSpan.Zero ? (PreviousTime + Duration) : gameTime.TotalGameTime;
            if (gameTime.TotalGameTime >= nextTime)
            {
                Iterate();
                PreviousTime = nextTime;
            }
        }

        public override bool IsVisibleAt(Microsoft.Xna.Framework.Rectangle fieldOfView)
        {
            return fieldOfView.Intersects(DestinationRectangle.ToXnaRectangle());
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Vector2 viewPoint)
        {
            Sprite currentSprite = CurrentSprite;
            Vector2 drawAt = Position - currentSprite.TileCenter - viewPoint;
            spriteBatch.Draw(currentSprite.Texture,
                new Rectangle(
                    (int)drawAt.X,
                    (int)drawAt.Y,
                    currentSprite.TileInTexture.Width,
                    currentSprite.TileInTexture.Height
                    ).ToXnaRectangle(),
                currentSprite.TileInTexture,
                Color,
                0f,
                Vector2.Zero,
                Flip,
                RealOrder);
        }

        public override XElement ToXml()
        {
            XElement xel = base.ToXml();
            xel.Add(
                new XAttribute("state", State),
                new XAttribute("next_state", NextState),
                new XAttribute("frame", Frame),
                new XAttribute("loop", Loop),
                new XAttribute("flip", Flip),
                new XAttribute("color", ColorHelper.ColorToHex(Color))
                );
            foreach (string key in States.Keys)
            {
                xel.Add(new XElement("State",
                    new XAttribute("key", key),
                    new XAttribute("group", States[key].Group),
                    new XAttribute("name", States[key].Name)));
            }
            return xel;
        }
    }
}
