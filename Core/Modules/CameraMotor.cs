﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using PorohGLib;
using PorohGLib.Resources;
using PorohGLib.Resources.Graphics;
using PorohGLib.Helpers;
using PorohGLib.Core.Objects;
using System.Xml.Linq;
using System.Globalization;

namespace PorohGLib.Core.Modules
{
    public class CameraMotor : GameModule
    {
        public override string Label { get { return "CameraMotor"; } }

        private GameObject Target;
        private float Left = 0, Right = 0, Top = 0, Bottom = 0;

        public CameraMotor(GameObject obj)
            : base(obj)
        {
            
        }

        public CameraMotor(XElement xel, GameObject obj)
            :base(xel, obj)
        {
            Left = float.Parse(xel.Attribute("left").Value, CultureInfo.InvariantCulture.NumberFormat);
            Right = float.Parse(xel.Attribute("right").Value, CultureInfo.InvariantCulture.NumberFormat);
            Top = float.Parse(xel.Attribute("top").Value, CultureInfo.InvariantCulture.NumberFormat);
            Bottom = float.Parse(xel.Attribute("bottom").Value, CultureInfo.InvariantCulture.NumberFormat);
        }

        public override void ThrowLinks()
        {
            Target = (GameObject)IdPool.GetEntityById(Links["target"]);
            base.ThrowLinks();
        }

        public void SetTarget(GameObject target)
        {
            Target = target;
        }

        public void SetBounds(float bounds)
        {
            SetBounds(bounds, bounds, bounds, bounds);
        }

        public void SetBounds(float left, float right, float top, float bottom)
        {
            Left = left;
            Right = right;
            Top = top;
            Bottom = bottom;
        }

        public override void Update(GameTime gameTime)
        {
            Vector2 offset = Target.Position - Object.Position;
            Vector2 correct = Vector2.Zero;
            if (offset.X > Right)
                correct.X = offset.X - Right;
            if (offset.X < -Left)
                correct.X = offset.X + Left;
            if (offset.Y > Bottom)
                correct.Y = offset.Y - Bottom;
            if (offset.Y < -Top)
                correct.Y = offset.Y + Top;

            Object.Move(correct);

            base.Update(gameTime);
        }

        public override XElement ToXml()
        {
            XElement xel = base.ToXml();
            XElement xlink = new XElement("Links",
                new XElement("Link",
                    new XAttribute("label", "target"),
                    new XAttribute("id", Target.Id)));
            xel.Add(xlink,
                new XAttribute("left", Left.ToString(CultureInfo.InvariantCulture.NumberFormat)),
                new XAttribute("right", Right.ToString(CultureInfo.InvariantCulture.NumberFormat)),
                new XAttribute("top", Top.ToString(CultureInfo.InvariantCulture.NumberFormat)),
                new XAttribute("bottom", Bottom.ToString(CultureInfo.InvariantCulture.NumberFormat)));
            return xel;
        }
    }
}
